SDAIA Project
=============

CICI: Secure Data Architecture: Shared Intelligence Platform for Protecting our National Cyberinfrastructure
ACI Award Number:1547249

-----------------------

Ansible roles found in site.yml.  Comment out roles not needed.

You can run `install.sh`, which requires and runs Ansible locally. Or deploy using your own Ansible instance via SSH. Install requires root.

Deploys SDAIA software on Ubuntu 16 and CentOS 7 (beta).

Report bugs to alexw1@illinois.edu
